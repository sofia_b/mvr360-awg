﻿#pragma strict

//Loading effect
var loading : boolean = false;
var loadingTexture : Texture;
var size : float = 70.0;
private var rotAngle : float = 0.0;
var rotSpeed : float = 300.0;

 var newTime: float = 0;

function Update () {

	if(loading){
	newTime +=Time.deltaTime;
		rotAngle += rotSpeed * Time.deltaTime;
		 if(newTime <= 3){
 		print("NewTime = " + newTime);
 	
}else{
		   loading = false;
		  newTime = 0;
		  }
		
		  
	}	
}

function OnGUI() {
	if(loading){

		var pivot : Vector2 = Vector2(Screen.width/2, Screen.height/2);
		GUIUtility.RotateAroundPivot(rotAngle%360,pivot);
		GUI.DrawTexture(Rect ((Screen.width - size)/4 , (Screen.height - size)/4, size, size), loadingTexture); 
		 

	}
}
