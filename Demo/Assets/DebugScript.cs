﻿using UnityEngine;
using System.Collections;

public class DebugScript : MonoBehaviour {
	//SMB{
	public string txt;
	public string titleTxt;
	public bool showTitle;
	public int maxLength;

	// Use this for initialization
	void Start () {
		InitApply ();
		if (maxLength < 1) {
			maxLength=1000;
		}
	}

	public void InitApply(){
		SetText (titleTxt);
	}

	public void SetText(string txtIn){
		string t = this.GetComponent<TextMesh> ().text;
		t = txtIn;
		this.GetComponent<TextMesh> ().text = t;
	}

	public void AddText(string txtIn){
		Debug.Log (txtIn);
		if (txt.Length >= maxLength) {
			txt = txtIn;
		} else {
			txt = txtIn + "\r\n" + txt;
		}
		if (showTitle) {
			this.GetComponent<TextMesh> ().text = titleTxt+"\r\n"+txt;
		} else {
			this.GetComponent<TextMesh> ().text = txt;
		}
	}
	//SMB}
}
