﻿using UnityEngine;
using System.Collections;

public class ObjectPersistentSetActive : MonoBehaviour {

	public GameObject ThisObject;
	public string TagToFind;

	void Start () {
		if (GameObject.FindGameObjectWithTag (TagToFind) != null) {
			if (GameObject.FindGameObjectWithTag (TagToFind) != gameObject) {
				Destroy (gameObject);
			}
		}
	}

	void Awake () {
		if (ThisObject == null) {
			DontDestroyOnLoad (gameObject);
			ThisObject = gameObject;
		} else {
			if (ThisObject != gameObject) {
				Destroy (gameObject);
			} else {
				//I'm are on my own object...
			}
		}
	}

}