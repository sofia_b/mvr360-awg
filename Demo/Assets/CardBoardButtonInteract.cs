﻿using UnityEngine;
using System.Collections;

public class CardBoardButtonInteract : MonoBehaviour {

	public GameObject VideoController;
	public bool OnlyOnPlayVideo;
	public bool IsPause;//If are true, the cardboard button act like an pause button, else, this act like an play button.
	public bool IsWellcomeButton;


	void Start() {
		VideoController = GameObject.FindGameObjectWithTag ("VideoController");
	}

	void OnEnable(){
		Cardboard.SDK.OnTrigger += TriggerPulled;
	}

	void OnDisable(){
		Cardboard.SDK.OnTrigger -= TriggerPulled;
	}

	void TriggerPulled() {
		Debug.Log("The trigger was pulled!");
		//this.GetComponent<VideoPlayController> ().Pause_Press ();
		if (OnlyOnPlayVideo) {
			if (IsPause) {
				VideoController.GetComponent<VideoPlayController> ().Pause_Press (gameObject);
			} else {
				VideoController.GetComponent<VideoPlayController> ().Play_Press (gameObject);
			}
		} else {
			if (IsWellcomeButton) {
				this.GetComponent<Welcome> ().GoNextScene ();
			}
		}
	}

}
