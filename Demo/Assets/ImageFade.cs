﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using UnityEngine.SceneManagement;

public class ImageFade : MonoBehaviour
{

    // the image you want to fade, assign in inspector
    public Image img;
    public Text texto;
  
    private void Start()
    {
        img.color = new Color(1, 1, 1, 0);
        fade();
    }
    public void fade()
    {
        // fades the image out when you click
        StartCoroutine(FadeIn());
        StartCoroutine(FadeOut());
        StartCoroutine(Next());

    }


    IEnumerator FadeIn()
    {
        // loop over 1 second
        for (float i = 0; i <= 1; i += Time.deltaTime)
        {
            // set color with i as alpha
            img.color = new Color(1, 1, 1, i);
            yield return null;
        }

    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(4);
        
        // loop over 1 second backwards
        for (float i = 1; i >= 0; i -= Time.deltaTime)
        {
            // set color with i as alpha
            img.color = new Color(1, 1, 1, i);
           
            yield return null;
        }


    }
    IEnumerator Next()
    {
        yield return new WaitForSeconds(6);
       
        Application.LoadLevel("Welcome");
        yield return null;
    }
        
    }

