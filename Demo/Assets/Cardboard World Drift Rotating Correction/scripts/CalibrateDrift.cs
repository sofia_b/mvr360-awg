﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CalibrateDrift : MonoBehaviour {

	public float calibrationTime=25;
	public Cardboard CB;
	public Transform head;
	public float driftSpeed=0;
	public bool calib;
	public Text timeText;
	public float variation;
	public float samplingTime=1;
	//public float driftAndgle;
	public float relYAngle;

	float rotY;
	float elapsed;
	long iter=0;

	// Use this for initialization
	void Start () 
	{
	
	}

	// Update is called once per frame
	//void FixedUpdate () 
	void nouse()//SMB: Add to null the entire opp.
	{

		elapsed+=Time.fixedDeltaTime;
		//elapsed2+=Time.fixedDeltaTime;


		Quaternion relative = Quaternion.Inverse(transform.rotation) * head.transform.rotation;
		relYAngle = relative.eulerAngles[1]; 

		if(relYAngle >180)
		{
			relYAngle-=360;
		}


		// if pressed reset calibration process
		if(CB.Triggered==true)//if(CB.Triggered==true)
		{
			driftSpeed=0;
			iter=0;
			elapsed=0;
			//elapsed2=0;
			rotY=0;
			calib=true;

			transform.rotation=Quaternion.Euler(0,head.rotation.eulerAngles[1],0);
		}



		// proceed with calibration
		if(elapsed<calibrationTime && calib==true )
		{
			// this is used to do something each second
			//if(elapsed2>samplingTime)
			//{
				
			//}

			timeText.text=""+ Mathf.Round(calibrationTime- elapsed);
		}
		else if(calib==true)
		{

			//this is the most important line, in which we obtain the drifting speed
			driftSpeed=relYAngle/calibrationTime;
			calib=false;

			Debug.Log("Rotating speed is:" +driftSpeed);
	
		}


		// apply drift speed correction
		if(elapsed >calibrationTime && calib==false )
		{
			//driftAndgle+=driftSpeed*Time.fixedDeltaTime;
			CB.transform.rotation=CB.transform.rotation*Quaternion.Euler(0,-driftSpeed*Time.fixedDeltaTime,0);

			//Debug.Log("Correction");
		}
				





	}




}
