﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Splash : MonoBehaviour {

	private float elapsedTime;

	// Use this for initialization
	void Start () {
		elapsedTime = 0;
	}
	
	// Update is called once per frame
	void Update () {
		elapsedTime += Time.deltaTime;

		if (elapsedTime > 8) {
			SceneManager.LoadScene ("Welcome");
		}
	}
}
