﻿using System;
using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RestClass :  MonoBehaviour
{
	private string results;

	public WWW GET(string url, System.Action<string> onComplete ) {

		WWW www = new WWW (url);
		StartCoroutine (WaitForRequest (www, onComplete));
		return www;
	}

	public WWW POST(string url, Dictionary<string,string> post, System.Action<string> onComplete) {
		WWWForm form = new WWWForm();

		foreach(KeyValuePair<string,string> post_arg in post) {
			form.AddField(post_arg.Key, post_arg.Value);
		}

		WWW www = new WWW(url, form);

		StartCoroutine(WaitForRequest(www, onComplete));
		return www;
	}

	private IEnumerator WaitForRequest(WWW www, System.Action<string> onComplete) {
		yield return www;
		// check for errors
		if (www.error == null) {
			results = www.text;
			Debug.Log ("SMB-RestClass: www.url = " + www.url);//SMB:This is the path from the JSON - This is THE NAME to the file for JSON

			onComplete(results);
		} else {
			Debug.Log (www.error);
		}
	}

	public string getResult(){
		return results;
	}
}
