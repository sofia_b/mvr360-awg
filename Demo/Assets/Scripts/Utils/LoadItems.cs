﻿using UnityEngine;
using System.Collections;




public class LoadItems : MonoBehaviour {

	public GameObject item;
	private RestClass rest;
	private int size = 10;
	private int maxColumnas = 6;
	private int maxFilas = 3;



	private void loadItems(){
		Debug.Log ("SMB-DownloadItems.....");
		int filas = size / maxColumnas;
		filas = size % maxColumnas != 0 ? filas + 1 : filas;

		int columnas = size / filas;
		if (filas > 1){
			columnas = size % filas != 0 ? columnas + 1 : columnas;
		}

		Debug.Log("filas:"+ filas);
		Debug.Log("columnas:"+ columnas);

		int leftDegree = 30 * (1 - columnas);
		for (int fila = 0; fila < filas && fila < maxFilas; fila++){
			for(int columna = 0; columna < columnas && columna < (size - (fila*columnas)); columna++){
				Instantiate (item, new Vector3 (0, 4*fila, 0), Quaternion.Euler(new Vector3 (0, leftDegree + (60 * columna), 0)));
				Debug.Log("degree:"+ leftDegree + (60 * columna));
			}
		}
		Debug.Log ("SMB-DownloadItems ENDs!!!!!");
	}




}
