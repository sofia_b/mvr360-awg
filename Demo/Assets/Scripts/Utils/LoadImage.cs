using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

public class LoadImage : MonoBehaviour {

	public GameObject thumbnail;
	public GameObject Title;
//	public string IdVideo;


	public void DownloadImage(string url)
	{   
			Debug.Log ("SMB-DownloadImage: Download the image: "+url);
			StartCoroutine(coDownloadImage(url));
			//IdVideo = url;


	}

	//SMB{
	public void SaveTextureToFile(Texture2D texturetosave, string path){
		// to store image
		var bytes = texturetosave.EncodeToJPG ();
		System.IO.File.WriteAllBytes (path, bytes);
	}

	public Texture2D LoadTextureFromFile(string path){
		// To get image
		var bytesRead = System.IO.File.ReadAllBytes (path);
		Texture2D myTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
		//Texture2D myTexture = new Texture2D (720, 271);
		myTexture.LoadImage(bytesRead);
		return myTexture;
	}
	public string DirectoryToDownload="DownloadTilesBack";//The Folder to storage the files download
	public string pathurl;

	public string GetDirectoryPath(){
		return Application.persistentDataPath + "/" + DirectoryToDownload;
	}

	public string ConvertURLtoFileName(string URLk){//COMPLETE
		string VideoName = URLk;
		VideoName = VideoName.Replace("http://","");
		VideoName = VideoName.Replace("/","-");
		VideoName = GetDirectoryPath() + "/" + VideoName;
		return VideoName;
	}

	public bool CheckDirectory(){
		return System.IO.Directory.Exists (GetDirectoryPath());
	}

	public void CreateDirectory(){
		if (!CheckDirectory ()) {
			System.IO.Directory.CreateDirectory(GetDirectoryPath());
		}
	}

	public void DeleteDirectory(){
		if (CheckDirectory ()) {
			System.IO.Directory.Delete(GetDirectoryPath(),true);
		}
	}

	public void ClearDirectory(){
		DeleteDirectory ();
		CreateDirectory ();
	}

	public bool FindFilesFromURL(string URLx){//To check
		bool Existing = System.IO.File.Exists(ConvertURLtoFileName(URLx));
		return Existing;
	}

	public void DeleteFileFromURL(string URLz){
		if(FindFilesFromURL(URLz)){
			System.IO.File.Delete(ConvertURLtoFileName(URLz));
		}
	}

 	//SMB}

	IEnumerator coDownloadImage(string imageUrl){
		//SMB{
		CreateDirectory ();
		if(FindFilesFromURL(imageUrl)){//if is pre-exist, only load the file, no reload the url if they exist
			GetComponent<Renderer> ().material.mainTexture = new Texture2D(4, 4, TextureFormat.RGB24, false);
			GetComponent<Renderer> ().material.mainTexture = LoadTextureFromFile (ConvertURLtoFileName (imageUrl));
			//GetComponent<Renderer> ().material.mainTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
			//return LoadTextFromFile (ConvertURLtoFileName (url));
		}else{

			Debug.Log ("SMB-DownloadImage: Init the coroutine: "+imageUrl);


		
//		while (true) {
			//Debug.Log ("Download: " + imageUrl);//SMB: Anulacion de debugs adiconales
	
			WWW www = new WWW(imageUrl);
		
			yield return www;
			//GetComponent<Renderer> ().material.mainTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
		
			GetComponent<Renderer> ().material.mainTexture = new Texture2D(4, 4, TextureFormat.RGB24, false);
		
			www.LoadImageIntoTexture(GetComponent<Renderer> ().material.mainTexture as Texture2D);
//		}
	
			SaveTextureToFile(GetComponent<Renderer> ().material.mainTexture as Texture2D, ConvertURLtoFileName(imageUrl));//SMB: This save the tile image
		}
	}
	//----SMB
	public float ResizeFont ;
	Vector3 CubeSize;

	public void ResizeTitle(){
		CubeSize.x = this.gameObject.GetComponent<Renderer> ().bounds.size.x;
	

		ResizeFont = 10;

		Title.GetComponent<TextMesh>().characterSize = ResizeFont;


	}

	void Update () {

	//	ResizeTitle ();	
	//	Debug.Log ("ObjectName = " + this.gameObject.name);
	//	Debug.Log ("ObjectSize = " + CubeSize.x);
	}
	//----SMB

}
