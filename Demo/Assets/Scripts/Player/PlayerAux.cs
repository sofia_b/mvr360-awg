﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerAux : MonoBehaviour {

	public MediaPlayerCtrl scrMedia;
	public string url;
	// Use this for initialization


	public static PlayerAux scriptdatos;
	
	void Awake () {
		if (scriptdatos == null) {
			scriptdatos = this;
			//da el control a esta instancia.
			DontDestroyOnLoad (gameObject);
			//se usa para generar una sola vez el objeto desde un scene y que no se pierdan sus satos
		} else if (scriptdatos != this) {
			Destroy(gameObject);
			//y que no se re-escriba si se usa como objeto desde cero nuevamente en otro scene.
		}
	}


	void Start () {
	}

	public void Loading () {
		//Debug.Log ("URL: " + url);

		Debug.Log("PLAYER AUX ===>>> Load ("+url+")....");
		scrMedia.Load(url);
		Debug.Log("PLAYER AUX ===>>> Load end!!!");
	}

	// Update is called once per frame
	void Update () {
	
	}
		
}
