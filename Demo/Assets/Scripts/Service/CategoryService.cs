﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class CategoryService : MonoBehaviour {
	//Inside  the category -VIDEOS
	private RestClass rest;
	public delegate void MyDelegate(ArrayList list);
	public MyDelegate myDelegate;
	string IdCat;
//	public string NN;


	public void Start(){
	//IdCat = this.gameObject.GetComponent<
	
	}

	//SMB{
	public string DirectoryToDownload="DownloadCategoriesJSON";//The Folder to storage the files download
	public string pathurl;
	public string JSONonPhone;


	public bool TestInternetConnection(){
		NetworkReachability NetReach;
		NetReach = Application.internetReachability;
		bool Result = false;
		switch (NetReach) {
		case NetworkReachability.ReachableViaLocalAreaNetwork:
			Debug.Log ("internetReachability = Can connect to Internet by LAN");
			Result = true;
			break;
		case NetworkReachability.ReachableViaCarrierDataNetwork:
			Debug.Log ("internetReachability = Can connect to Internet by CARRIER");
			Result = true;
			break;
		case NetworkReachability.NotReachable:
			Debug.Log ("internetReachability = NOT CONNECTED!!!!");
			break;
		default:
			Debug.Log ("internetReachability = ERROR!!!!");
			break;
		}
		return Result;
	}

	public string GetDirectoryPath(){
		return Application.persistentDataPath + "/" + DirectoryToDownload;
	}

	public string ConvertURLtoFileName(string URLk){//COMPLETE
		string VideoName = URLk;
		VideoName = VideoName.Replace("http://","");
		VideoName = VideoName.Replace("/","-");
		VideoName = GetDirectoryPath() + "/" + VideoName;
		return VideoName;
	}

	public bool CheckDirectory(){
		return System.IO.Directory.Exists (GetDirectoryPath());
	}

	public void CreateDirectory(){
		if (!CheckDirectory ()) {
			System.IO.Directory.CreateDirectory(GetDirectoryPath());
		}
	}

	public void DeleteDirectory(){
		if (CheckDirectory ()) {
			System.IO.Directory.Delete(GetDirectoryPath(),true);
		}
	}

	public void ClearDirectory(){
		DeleteDirectory ();
		CreateDirectory ();
	}

	public bool FindFilesFromURL(string URLx){//To check
		bool Existing = System.IO.File.Exists(ConvertURLtoFileName(URLx));
		return Existing;
	}

	public void DeleteFileFromURL(string URLz){
		if(FindFilesFromURL(URLz)){
			System.IO.File.Delete(ConvertURLtoFileName(URLz));
		}
	}

	public void SaveTextToFile(string texttosave, string pathfile){
		// to store image
		System.IO.File.WriteAllText(pathfile,texttosave);
		Debug.Log ("Saved JSON on phone!!!");
	}

	public string LoadTextFromFile(string pathfile){
		// To get image
		string myText = System.IO.File.ReadAllText (pathfile);
		return myText;
	}

	public string TryToLoadFileFromURL(string url){
		CreateDirectory ();
		if(FindFilesFromURL(url)){
			return LoadTextFromFile (ConvertURLtoFileName (url));
		}else{
			return "";
		}
	}
	public void TryToSaveJSONFileFromURL(){
		CreateDirectory ();
		if (FindFilesFromURL (pathurl)) {
			DeleteFileFromURL (pathurl);
			Debug.Log ("Pre-exist JSON on phone, delete to rewrite");
		} else {
		}
		SaveTextToFile (JSONonPhone, ConvertURLtoFileName (pathurl));
	}
	//SMB}

	public void read(int categoryId){
		Debug.Log ("SMB-CategoryServices: ReadFromWeb: http://udistcms.com/api/portals/59/category/"+ categoryId +"/contents");//SMB: This call every time when you open a category or simply access to the sphere category
		pathurl="http://udistcms.com/api/portals/59/category/"+ categoryId +"/contents";
		//SMB: Try to load JSON info previous saved on file from web (from the second start ald beyond!!)
		JSONonPhone=TryToLoadFileFromURL(pathurl);
		Debug.Log ("JSON on phone: "+JSONonPhone);
		//SMB: frist, test internet connection...
		if (TestInternetConnection ()) {
			rest = gameObject.AddComponent<RestClass>();
			rest.GET ("http://udistcms.com/api/portals/59/category/"+ categoryId +"/contents", onComplete);
		} else {
			if (JSONonPhone != "") {
				onCompleteDirectFromFile ();
			}
		}
	}

	public void onComplete(string r){
		if (r != JSONonPhone) {
			JSONonPhone = r;
			Debug.Log ("Write JSON on phone: "+JSONonPhone);
			TryToSaveJSONFileFromURL ();
		}

		//Debug.Log ("R: " + r);//SMB: Debug anulado
		var contents = JSON.Parse(r);
		//Debug.Log ("contents: " + contents.Count);//SMB: Debug anulado

		ArrayList result = new ArrayList ();

		for (int i=0; i<contents.Count; i++ ){

			var content = contents [i];
			Content cont = new Content ();

			cont.id = content ["ContentId"].AsInt;
			cont.title = content ["Title"].Value;
			cont.description = content ["Title"].Value;
			cont.thumbnail = content ["ThumbnailLargeUrl"].Value;
			cont.url = content ["Url"].Value;
			//SMB{
			//Debug.Log("XXX =====>>>>> URL: "+cont.url);
			//}SMB

			result.Add (cont);

		}

		myDelegate (result);
	}

	//SMB{
	public void onCompleteDirectFromFile(){
		var contents = JSON.Parse(JSONonPhone);
		//Debug.Log ("contents: " + contents.Count);//SMB: Debug anulado

		ArrayList result = new ArrayList ();

		for (int i=0; i<contents.Count; i++ ){

			var content = contents [i];
			Content cont = new Content ();

			cont.id = content ["ContentId"].AsInt;
			cont.title = content ["Title"].Value;
			cont.description = content ["Title"].Value;
			cont.thumbnail = content ["ThumbnailLargeUrl"].Value;
			cont.url = content ["Url"].Value;
			//SMB{
			//Debug.Log("XXX =====>>>>> URL: "+cont.url);
			//}SMB

			result.Add (cont);

		}

		myDelegate (result);
	}
	//SMB}
}

public class Content {
	public int id;
	public string thumbnail;
	public string url;
	public string title;
	public string description;
}