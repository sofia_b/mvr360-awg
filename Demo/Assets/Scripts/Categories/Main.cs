﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Main : MonoBehaviour {

	PortalService portalService;

	public GameObject item;//Cube0---SMB
	public GameObject home;
	public GameObject back;

	private ArrayList categories;
	private int maxColumnas = 9;
	private int maxFilas = 3;








	//SMB{
	public void SaveTextureToFile(Texture2D texturetosave, string path){
		// to store image
		var bytes = texturetosave.EncodeToJPG ();
		System.IO.File.WriteAllBytes (path, bytes);
	}

	public Texture2D LoadTextureFromFile(string path){
		// To get image
		var bytesRead = System.IO.File.ReadAllBytes (path);
		Texture2D myTexture = new Texture2D(4, 4, TextureFormat.DXT1, false);
		//Texture2D myTexture = new Texture2D (720, 271);
		myTexture.LoadImage(bytesRead);
		return myTexture;
	}
	public string DirectoryToDownload="DownloadCatBG";//The Folder to storage the files download
	public string pathurl;

	public string GetDirectoryPath(){
		return Application.persistentDataPath + "/" + DirectoryToDownload;
	}

	public string ConvertURLtoFileName(string URLk){//COMPLETE
		string VideoName = URLk;
		VideoName = VideoName.Replace("http://","");
		VideoName = VideoName.Replace("/","-");
		VideoName = GetDirectoryPath() + "/" + VideoName;
		return VideoName;
	}

	public bool CheckDirectory(){
		return System.IO.Directory.Exists (GetDirectoryPath());
	}

	public void CreateDirectory(){
		if (!CheckDirectory ()) {
			System.IO.Directory.CreateDirectory(GetDirectoryPath());
		}
	}

	public void DeleteDirectory(){
		if (CheckDirectory ()) {
			System.IO.Directory.Delete(GetDirectoryPath(),true);
		}
	}

	public void ClearDirectory(){
		DeleteDirectory ();
		CreateDirectory ();
	}

	public bool FindFilesFromURL(string URLx){//To check
		bool Existing = System.IO.File.Exists(ConvertURLtoFileName(URLx));
		return Existing;
	}

	public void DeleteFileFromURL(string URLz){
		if(FindFilesFromURL(URLz)){
			System.IO.File.Delete(ConvertURLtoFileName(URLz));
		}
	}

	IEnumerator GetBG(string UrlBG) {
		if(!FindFilesFromURL(UrlBG)){
			WWW www = new WWW(UrlBG);
			yield return www;
			SaveTextureToFile(www.texture as Texture2D, ConvertURLtoFileName(UrlBG));//SMB: This save the tile image
			Debug.Log (ConvertURLtoFileName(UrlBG));
		}
	}

	void DownloadAllBG(ArrayList listelements){

		Debug.Log (portalService.JSONonPhone);

		CreateDirectory ();

		foreach (Category Ct in listelements) {
			Debug.Log (Ct.BgUrl2);
			StartCoroutine (GetBG (Ct.BgUrl2));
			Debug.Log (ConvertURLtoFileName(Ct.BgUrl2));

		}
		Debug.Log (listelements);

	}

	//SMB}






	// Use this for initialization
	void Start () {
		portalService = gameObject.AddComponent<PortalService>();
		portalService.myDelegate = ShowCategories;
		portalService.read();
//		GetComponent<Cardboard>().OnTilt += () => { Application.Quit(); };
//		GetComponent<Cardboard>().OnBackButton += ()=>{Application.Quit(); };

		SelectItem homeSelectItem = (SelectItem)home.GetComponent(typeof(SelectItem));
		homeSelectItem.itemSelected = OpenCategory;

		SelectItem backSelectItem = (SelectItem)back.GetComponent(typeof(SelectItem));
		backSelectItem.itemSelected = OpenCategory;
	}
	
	// Update is called once per frame
/*	Vector3 ItemSize;
	float MaxWidth;
	float MinWidth;
	float MaxHeight;
	float MinHeight;*/

	int CountText;
	int lineLength;
	string textCat;

	void Update () {
	/*	ItemSize =item.GetComponent<Transform>().localScale;
	//	Debug.Log("ItemSize = " + ItemSize);
		MaxWidth = ItemSize.x;
	//	Debug.Log("ItemSizeX = " + ItemSize.x);
		MaxHeight = ItemSize.y;
		Debug.Log(" CountText = " + CountText);*/
	}

	//SMB{
	public float heightButton;
	//SMB}

	void ShowCategories(ArrayList list){

		DownloadAllBG (list);

		categories = list;
		int size = categories.Count;

		int filas = size / maxColumnas;
		filas = size % maxColumnas != 0 ? filas + 1 : filas;

		int columnas = size / filas;
		if (filas > 1){
			columnas = size % filas != 0 ? columnas + 1 : columnas;
		}

		//Debug.Log("filas:"+ filas);//SMB: Debug anulado
		//Debug.Log("columnas:"+ columnas);//SMB: Debug anulado

		int leftDegree = 20 * (1 - columnas) + 180;
		for (int fila = 0; fila < filas && fila < maxFilas; fila++){
			for(int columna = 0; columna < columnas && columna < (size - (fila*columnas)); columna++){

				int index = columna + fila * columnas;

				Category cat = categories [index] as Category;
				GameObject card = Instantiate (item, new Vector3 (0, (4*fila)+heightButton, 0), Quaternion.Euler(new Vector3 (0, leftDegree + (40 * columna), 0)))  as GameObject;
//				card.GetComponent (typeof(TextMesh));
				TextMesh title = (TextMesh)card.GetComponentInChildren (typeof(TextMesh));
				title.text = cat.name;
				//SMB{
				//Debug.Log("ZZ ===>>> ITEM: '"+item+"' Category: '"+cat.id+" - "+cat.name+" - "+cat.url+"' card: '"+card.name+"'");
				card.name="Cat"+columna+"-"+cat.id+"-"+cat.name;
				card.tag="Categories";
				//}SMB
				/*
				   if(input[i]==' '){
     				input[i]="\r\n";
 					}//end if
				 */
			//	title.offsetZ = 10;
				//title.tabSize= 2;
				//title.lineSpacing = 10;





				LoadImage imageLoader = (LoadImage) card.GetComponent(typeof(LoadImage));
				imageLoader.DownloadImage(cat.url);
				SelectItem selector = (SelectItem)card.GetComponent (typeof(SelectItem));
				selector.index = index;
				selector.itemSelected = OpenCategory;


				//Debug.Log("degree:"+ leftDegree + (40 * columna));//SMB: Limpieza de Logs
			}
		}
	}

	public void OpenCategory(int index){

		if (index == -1){
			SceneManager.LoadScene ("Welcome");
			return;
		}
		if (index == -2){
			SceneManager.LoadScene ("Welcome");
			return;
		}
		Category cat = categories [index] as Category;
		PlayerPrefs.SetInt("Category_id", cat.id);
		PlayerPrefs.SetString("Category_URL2", cat.BgUrl2);

		//SMB{
		//Debug.Log ("MAIN ===>>> Start the Player...");
		//}SMB
		Cardboard.SDK.Recenter();//SMB: Act to re-center the camera
		SceneManager.LoadScene ("Videos");
	}
}
