﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Net;
using System;

public class Welcome : MonoBehaviour {

	// Use this for initialization
	void Start () {
		DownloadFile ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void GoNextScene() {
		//Debug.Log( "Atrapo el Click" );//SMB: Debug anulado

		SceneManager.LoadScene ("Pre_Categories");
	}

	void DownloadFile()
	{
		//Debug.Log( "Empiezo la descarga" );//SMB: Debug anulado
		WebClient client = new WebClient();
		client.DownloadFileCompleted += (s, e) => {
			
			string fileIdentifier= ((System.Net.WebClient)(s)).QueryString["file"];
			//			Debug.Log ( Application.dataPath + ".." + "/" + "sibling_path");
//			Debug.Log (fileIdentifier);
//			Debug.Log (e.Error);

//			PlayerPrefs.SetString("ContentURL", "GuerraHotDeAlmohadas.mp4");
//			SceneManager.LoadScene ("Player");	
		};
		client.DownloadProgressChanged +=  (s, e) =>
		{
			//Debug.Log (e.ProgressPercentage);//SMB: Debug anulado
		};

		//Debug.Log (Application.persistentDataPath);//SMB: Debug anulado
		client.DownloadFileAsync(new Uri("http://udistcms.com/cdn/content/2/2909/file.mp4"), Application.persistentDataPath + "/" + "GuerraHotDeAlmohadas.mp4");
	}

	void DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e){
		if (e.Error == null){
			GoNextScene ();
		}
	}
}
