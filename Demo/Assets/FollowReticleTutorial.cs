﻿using UnityEngine;
using System.Collections;

public class FollowReticleTutorial : MonoBehaviour {


	public bool FollowReticleDefault;
	public bool FollowX;
	public bool FollowY;
	public bool FollowZ;
	public float AngleXMax;
	public float AngleYMax;
	public GameObject ObjectToFollow;
	public Vector3 OriginalEulerAngles;
	public Vector3 ActualEulerAngles;
	public Vector3 RefReticleEulerAngles;
	public Vector3 ReticleEulerAngles;

	// Use this for initialization
	void Start () {
		OriginalEulerAngles = this.transform.eulerAngles;
	}

	// Update is called once per frame
	void Update () {
		if ((ObjectToFollow == null) && (FollowReticleDefault)) {
			if (GameObject.FindGameObjectWithTag ("Reticle") != null) {
				ObjectToFollow = GameObject.FindGameObjectWithTag ("Reticle");
			}
		}

		ReticleEulerAngles = ObjectToFollow.transform.eulerAngles;
		if (FollowX) {
			if(Mathf.DeltaAngle (ReticleEulerAngles.x, RefReticleEulerAngles.x)>AngleXMax){
				RefReticleEulerAngles.x=RefReticleEulerAngles.x-(Mathf.DeltaAngle (ReticleEulerAngles.x, RefReticleEulerAngles.x)-AngleXMax);
			}else{
				if(Mathf.DeltaAngle (ReticleEulerAngles.x, RefReticleEulerAngles.x)<(-AngleXMax)){
					RefReticleEulerAngles.x=RefReticleEulerAngles.x-(Mathf.DeltaAngle (ReticleEulerAngles.x, RefReticleEulerAngles.x)+AngleXMax);
				}else{
				}
			}
		} else {
			RefReticleEulerAngles.x=0f;
		}
		if (FollowY) {
			if(Mathf.DeltaAngle (ReticleEulerAngles.y, RefReticleEulerAngles.y)>AngleYMax){
				RefReticleEulerAngles.y=RefReticleEulerAngles.y-(Mathf.DeltaAngle (ReticleEulerAngles.y, RefReticleEulerAngles.y)-AngleYMax);
			}else{
				if(Mathf.DeltaAngle (ReticleEulerAngles.y, RefReticleEulerAngles.y)<(-AngleYMax)){
					RefReticleEulerAngles.y=RefReticleEulerAngles.y-(Mathf.DeltaAngle (ReticleEulerAngles.y, RefReticleEulerAngles.y)+AngleYMax);
				}else{
				}
			}
		}else{
			RefReticleEulerAngles.y=0f;
		}
		if (FollowZ) {
		}else{
			RefReticleEulerAngles.z=0f;
		}
		ActualEulerAngles = OriginalEulerAngles+ RefReticleEulerAngles;
		this.transform.eulerAngles = ActualEulerAngles;
	}
}
